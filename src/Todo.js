import React, { useState, useCallback } from 'react';
import styled from 'styled-components'

import TodoList from './components/TodoList';
import AddNewTodoForm from './components/AddNewTodoForm';

import { getTodoItemsFromLocalStorage, saveTodoItemsToLocalStorage } from './helpers'

const Container = styled.div`
width: 300px;
margin: 10px auto;
font-family: Arial, Helvetica, sans-serif;
font-size: 13px;

h1 {
  text-align: center
}
`

const Todo = () => {
  const [todoItems, setTodoItems] = useState(getTodoItemsFromLocalStorage('todo') || [])

  const addTodoHandler = useCallback(todo => {
    let latestTodoItem = null
    if (todoItems.length === 1) {
      latestTodoItem = todoItems[0]
    }
    else if (todoItems.length > 1) {
      const todoItemsDescendingSortedById = todoItems.sort((a, b) => a.id > b.id)
      latestTodoItem = todoItemsDescendingSortedById[0]
    }
    
    const newTodoItems = [
      {
        id: latestTodoItem ? latestTodoItem.id + 1 : 0,
        todo,
      },
      ...todoItems,
    ]

    setTodoItems(newTodoItems)

    saveTodoItemsToLocalStorage('todo', newTodoItems)
  }, [todoItems])

  const removeTodoHandler = useCallback(id => {
    const newTodoItems = todoItems.filter(todoItem => todoItem.id !== id)

    setTodoItems(newTodoItems)

    saveTodoItemsToLocalStorage('todo', newTodoItems)
  }, [todoItems])

  return (
    <Container>
      <div className="todo">
        <h1>ToDo Luby</h1>

        <AddNewTodoForm
          onAddTodo={addTodoHandler}
        />

        <TodoList
          todoItems={todoItems}
          onRemoveTodo={removeTodoHandler}
        />
        <hr></hr>

      </div>
    </Container>
  );
}

export default Todo;