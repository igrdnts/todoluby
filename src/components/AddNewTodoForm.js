import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components'

const StyledButton = styled.button`
    float: right;
    background: palevioletred;
    color: #FFF;
    border-radius: 3px;
    border: 2px solid palevioletred;
    padding: 5px 5px;
    outline: none;
    cursor: pointer
`

const FormInput = styled.input`
    width: 65%;
    outline: none;
    font-size: 13px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 10px;
`

const AddNewTodoForm = ({ onAddTodo }) => {
  const formik = useFormik({
    validateOnChange: false,
    validateOnBlur: false,
    initialValues: {
      todo: '',
    },
    validationSchema: Yup
      .object()
      .shape({
        todo: Yup.string()
          .min(5, 'Insira uma ação com mais de 5 caracteres.')
          .required('Campo To-Do vazio.')
        }
      ),
    onSubmit: (values, { resetForm }) => {
      onAddTodo(values.todo);

      resetForm();
    },
  })

  const errorKeys = Object.keys(formik.errors);

  const aFormikError = errorKeys.length > 0 ? formik.errors[errorKeys[0]] : null;

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <FormInput
          placeholder="Insira um novo To-Do..."
          id="todo"
          name="todo"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.todo}
          autoComplete="off"
        />
        <StyledButton type="submit">Adicionar</StyledButton>
        {aFormikError && <span>{aFormikError}</span>}
      </form>
      <hr></hr>
    </div>
  )
};

export default AddNewTodoForm;