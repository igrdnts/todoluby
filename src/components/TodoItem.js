import React, { useCallback } from 'react';
import styled from 'styled-components'

const StyledList = styled.li`
    list-style: none;
    overflow: hidden;
    width: 100%;
    margin-bottom: 10px
`
const StyledLabel = styled.label`
    float: left;
    cursor: pointer
`

const StyledButton = styled.button`
    float: right;
    background: palevioletred;
    color: #FFF;
    border-radius: 3px;
    border: 2px solid palevioletred;
    padding: 3px 10px;
    outline: none;
    cursor: pointer
`

const TodoItem = ({ todo, id, onRemoveTodo }) => {
  const removeTodoHandler = useCallback(() => onRemoveTodo(id), [id, onRemoveTodo])

  return (
    <StyledList>
        <StyledLabel><label>{todo}</label></StyledLabel>
        <StyledButton onClick={removeTodoHandler}>Apagar</StyledButton>
    </StyledList>
  )
};

export default TodoItem;