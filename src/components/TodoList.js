import React from 'react';

import TodoItem from './TodoItem';

const TodoList = ({ todoItems, onRemoveTodo }) => (
  <div>
    {
      todoItems && 
      Array.isArray(todoItems) && 
      todoItems.length > 0 &&
      todoItems.map(({ id, todo }) => (
        <TodoItem
          id={id}
          todo={todo}
          onRemoveTodo={onRemoveTodo}
        />
      ))
    }
  </div>
);

export default TodoList;